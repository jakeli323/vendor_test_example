# Host: localhost  (Version: 5.5.47)
# Date: 2016-07-20 21:34:12
# Generator: MySQL-Front 5.3  (Build 4.234)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "think_blog"
#

DROP TABLE IF EXISTS `think_blog`;
CREATE TABLE `think_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_title` varchar(255) NOT NULL,
  `blog_author` varchar(50) NOT NULL,
  `blog_content` varchar(255) NOT NULL,
  `blog_create_time` varchar(255) NOT NULL,
  `blog_update_time` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Data for table "think_blog"
#

/*!40000 ALTER TABLE `think_blog` DISABLE KEYS */;
INSERT INTO `think_blog` VALUES (1,'第一篇博客','pangPython','这是一篇博客，使用thinkphp开发：)','1469020818','1469020818'),(2,'测试博客','admin','这里是我的内容，哈哈哈哈哈哈','1469020818','1469020818'),(3,'正在学习php的孩子们','root','好好学习，天天向上，。PHP是世界上最好的语言！！哈哈','1469020818','1469020818'),(4,'今天下大雨了哦哦','pangPython','在看韩剧诶看见很快就拉黑的房间里萨奥斯U盾哦你按速度','1469020818','1469020818'),(5,'这是一首简单的下情歌','pangPython','是的范德萨发发呆发个好的法国恢复到韩国的风格程序下载v','1469020818','1469020818'),(6,'一场大雨大雨','pangPython','今天又继续下雨了，还是咋喜爱阿萨德加拉斯','1469020818','1469020818');
/*!40000 ALTER TABLE `think_blog` ENABLE KEYS */;

#
# Structure for table "think_user"
#

DROP TABLE IF EXISTS `think_user`;
CREATE TABLE `think_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `user_sex` int(11) DEFAULT NULL,
  `user_tel` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_address` varchar(255) DEFAULT NULL,
  `user_birth` varchar(255) DEFAULT NULL,
  `user_jointime` varchar(255) DEFAULT NULL,
  `user_passwd` varchar(255) DEFAULT NULL,
  `user_signature` varchar(255) DEFAULT NULL,
  `user_hobby` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

#
# Data for table "think_user"
#

INSERT INTO `think_user` VALUES (1,'thinkphp',1,'15700000000','emial@email.com','山东省济南市****路','1111111','111111','qqq',NULL,NULL,1),(2,'pangPython',1,'15700000000','email@emial.com','山东省济南市','201607096',NULL,'123456','nihao','上速度速度',1),(3,'test',0,'15722222222','pangPython@163.com','北京中关村','19931212','12999922','21232f297a57a5a743894a0e4a801fc3','这个人特别懒，什么都没写','抽烟喝酒烫头',1),(4,'你是什么鬼',0,'15888889999','admin@haha.com','美国硅谷','19801002','213123','63a9f0ea7bb98050796b649e85481845','个性前景','啪啪啪',1);